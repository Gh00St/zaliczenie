<?php
class Samochod {
	public $marka;
	public $model;
	public $rokProdukcji;
}
$samochod = new Samochod();
$samochod->marka = 'Volkswagen';
$samochod->model = 'Golf VI';
$samochod->rokProdukcji = 2009;

echo $samochod->marka . ' ' . $samochod->model . ' ' . $samochod->rokProdukcji;

"<br>";
$samochod = array ("marka", "model", "rokProdukcji");
$samochod['marka'] = 'Volkswagen';
$samochod['model'] = 'Golf VI';
$samochod['rokProdukcji'] = 2009;
echo $samochod['marka'] . ' ' . $samochod['model'] . ' ' . $samochod['rokProdukcji'];
echo "<br>";

class Auto {
	public $marka;
	public $model;
	public $iloscPaliwa = 50;
	public $spalanie = 2.5;
	public function tankuj ($ileLitrow) {
		$this->iloscPaliwa += $ileLitrow;
	}
	public function setSpalanie($spalanie) {
		$wartoscSpalania = intval($spalanie);
	if ($wartoscSpalania > 0 && $wartoscSpalania < 100)
		$this->spalanie = $wartoscSpalania;
	}
	public function ilePrzejedzie() {
		if (empty($this->spalanie))
			return 0;
		return $this->iloscPaliwa / $this->spalanie * 100;
	}
}

?>
